package com.example.testprojet;

public class calculatrice {

    public Integer additionner(Integer nb1, Integer nb2)
    {
        return nb1+nb2;
    }

    public Integer multiplier(Integer nb1, Integer nb2)
    {
        return nb1*nb2;
    }

    public int diviser(Integer nb1, Integer nb2) throws ArithmeticException
    {
        return nb1/nb2;
    }
}
