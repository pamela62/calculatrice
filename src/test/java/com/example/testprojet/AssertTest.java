package com.example.testprojet;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.awt.*;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class AssertTest {

    @Test
    void inegaliteNombre(){
        assertNotEquals(5,4,"5 et 4 ne sont pas égaux");
    }

    @Test
    void inegaliteList(){
        List<String> liste1 = Arrays.asList("a","B");
        List<String> liste2 = Arrays.asList("c","d");

        assertNotEquals(liste1, liste2);

    }

    @Test
    void testTrue(){
        assertTrue(true);
    }

    @Test
    void egaliteObject(){
        String a = new String("hello");
        String b = new String("hello");

        String c = b;

        //Ne marche pas car deux instances différentes
        //assertNotSame(a,b);

        //marche car la même instance
        assertNotSame(c,b);

    }

    @Test
    void testGroup(){
        Dimension rect = new Dimension(400,500);
        assertAll("On test les dimensions du rectangle",()->  {
           assertTrue(rect.getHeight() == 500, "La hauteur est égale à 500");
        }, () -> {
            assertTrue(rect.getWidth() == 400, "la largeur doit être égale à 400");
        });
    }

    @Test
    void testNull(){
        Object test = null;
        //assertNull(test);
        assertNotNull(test);
    }

    @Test
    void checkLists(){
        List<String> liste1 = Arrays.asList("jiji@gmail.com", "jojo@gmail.com");
        List<String> liste2 = Arrays.asList("bibi@gmail.com", "bobo@gmail.com");
        List<String> liste3 = Arrays.asList("(.*)@(.*)", "(.*)@(.*)");

        assertAll("", () -> {
            assertLinesMatch(liste1, liste2, "Les listes ne correspondent pas");
        },()-> {
            assertLinesMatch(liste3, liste2, "Les listes ne correspondent pas");
        });

        List<String> liste4 = Arrays.asList("fifi@gmail.com","fifi", "dodo@yahoo.fr");
        List<String> liste5 = Arrays.asList("(.*)@(.*)", ">> >>", "(.*)@(.*)",">> >>");

        assertLinesMatch(liste5, liste4, "Les listes ne correspondent pas");


    }


}
