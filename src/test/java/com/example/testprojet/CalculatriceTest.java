package com.example.testprojet;

import net.bytebuddy.asm.Advice;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CalculatriceTest {
    private calculatrice calculatrice;

    /*permet d'intialiser a zero*/
    @BeforeEach
    void setUp(){
        this.calculatrice = new calculatrice();
    }

    @Test
    @DisplayName("L'addition doit fonctionner")
    void testAdditionner(){
        assertEquals(13, calculatrice.additionner(8,5), "L'addition de 10 et 5 doit donner 15");

    }

    @Test
    @DisplayName("La multiplication doit fonctionner")
    void testMultiplier(){
        assertEquals(40, calculatrice.multiplier(8,5), "La multiplication de 8 et 5 doit donner 40");
    }

    @Test
    @DisplayName("La multiplication par zéro doit donner zéro")
    void multiplierParZero(){
        assertEquals(0, calculatrice.multiplier(42,0), "42*0 = 0");
    }

    @Test
    @DisplayName("La division par zero doit échouer")
    void divisierparzero(){
        assertThrows(ArithmeticException.class, () -> calculatrice.diviser(5,0));
    }

    @RepeatedTest(5)
    @DisplayName("La division doit fonctionner plusieurs fois à la suite")
    void divisionTest()
    {
        assertEquals(5, calculatrice.diviser(10,2));
    }

}
