package com.example.testprojet;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class CycleDeVie {
    CycleDeVie(){}

    @BeforeAll
    static void initBeforeAll(){
        System.out.println("Execution unique avant tous les tests");
    }

    @BeforeEach
    void initBeforeEach(){
        System.out.println("Execution avant chaque méthode de tests");
    }

    @AfterEach
    void initAfterEach(){
        System.out.println("Execution apres chaque méthode de tests");
    }

    @AfterAll
    static void initAfterAll(){
        System.out.println("Execution apres le dernier tests");
    }

    @Test
    void testUn(){
        System.out.println("premier test");
        assertTrue(true);
    }

    @Test
    void testDeux(){
        System.out.println("deuxieme test");
        assertTrue(true);
    }
}
