package com.example.testprojet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HelloTest {

    Hello hello;

    @BeforeEach
    void setUp(){
        this.hello = new Hello();
    }


    @Test
    @DisplayName("L'application doit dire bonjour")
    void sayHello() {
        assertEquals("Coucou !", hello.sayHello());
    }
}